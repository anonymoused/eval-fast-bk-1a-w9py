﻿# ForCode API project

* The way this project starts the donnet code in the container didn’t work well with a volume being added to the container. That's how my current job works with containers so I added it out habit. It wasn't needed do I didn't fight with it.'
* The tests are all lumped into one test to maintain order. That isn't how I would normally do it, but I've never used xUnit to test APIs before. I've used fitnesse or Selenium (not my favorite) but didn't feel like adding that overhead to this project.
* The test are all happy path. Many more could be added for non-happy path testing. 
* I didn't make the controller methods async as it wasn't needed for the expressed requirements.
* As always, tests saved by bacon as they caught my bugs early. 
* This project was not insignificant, but I enjoyed doing it. 
* Run the containers to run the acceptance tests.
