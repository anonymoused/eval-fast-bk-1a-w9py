using FortCode;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Net.Http;
using Xunit;
using FortCode.Models;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace FortCodeIntegrationTest
{
   /// <summary>
   /// NOTE: 
   /// Docker containers need to be running for this test to work as it is an acceptance test. 
   /// Ideally there would be one "Fact" per test, but I'm new to xUnit and didn't add the needed code to order tests
   /// and run stuff in stetup/teardown. 
   /// </summary>
   public class UnitTest1
   {
      private readonly string _url = "http://localhost:8100/api";

      [Fact]
      public void TestAll()
      {
         UserLogin edLogin = new UserLogin("ed", "password");
         UserLogin fredLogin = new UserLogin("fred", "buzz");
         UserLogin tedLogin = new UserLogin("ted", "fizz");

         UserModel edModel = new UserModel("ed", "password", "ed@junk.blah");
         UserModel fredModel = new UserModel("fred", "buzz", "fred@junk.blah");
         UserModel tedModel = new UserModel("ted", "fizz", "ted@junk.blah");

         CityInfo phillyCity = new CityInfo("Philly", "USA");
         CityInfo nycCity = new CityInfo("NYC", "USA");

         BarInfo secondstPhilly = new BarInfo("Philly", "2nd st", "beer");
         BarInfo thirdsPhilly = new BarInfo("Philly", "3rd st", "wine");
         BarInfo secondstNCY = new BarInfo("NYC", "Sams", "beer");
         BarInfo thirdsNYC = new BarInfo("NYC", "Petes", "wine");
         BarInfo fouthstNCY = new BarInfo("NYC", "Ralphs", "gin");

         string addUser = $"{_url}/user/adduser";
         string getCity = $"{_url}/user/getcities";
         string addCity = $"{_url}/user/addcity";
         string deleteCity = $"{_url}/user/deletecity";
         string addBar = $"{_url}/user/addbar";
         string deleteBar = $"{_url}/user/deletebar";
         string getBars = $"{_url}/user/getbars";
         string sharedBars = $"{_url}/user/sharedBars";

         ClearDB();
         
         // Create Ed via adduser
         RunTestScenario(edModel, edLogin, addUser, "ed added");

         // Add philly via addcity
         RunTestScenario(phillyCity, edLogin, addCity, "{\"name\":\"ed\",\"city\":\"Philly\",\"country\":\"USA\"}");

         // Verifiy phill via getcities
         RunTestScenario(edLogin, getCity, "[{\"Name\":\"ed\",\"City\":\"Philly\",\"Country\":\"USA\"}]");

         // delete philly via deletecity
         RunTestScenario(phillyCity, edLogin, deleteCity, "{\"name\":\"ed\",\"city\":\"Philly\",\"country\":\"USA\"}");

         // verify no city via getcities
         RunTestScenario(edLogin, getCity, "[]");

         // add philly via addcity
         RunTestScenario(phillyCity, edLogin, addCity, "{\"name\":\"ed\",\"city\":\"Philly\",\"country\":\"USA\"}");
         
         // add nyc via addcity
         RunTestScenario(nycCity, edLogin, addCity, "{\"name\":\"ed\",\"city\":\"NYC\",\"country\":\"USA\"}");
         
         // verify philly/nyc via getcities
         RunTestScenario(edLogin, getCity, "[{\"Name\":\"ed\",\"City\":\"Philly\",\"Country\":\"USA\"},{\"Name\":\"ed\",\"City\":\"NYC\",\"Country\":\"USA\"}]");

         // add bar to philly 
         RunTestScenario(secondstPhilly, edLogin, addBar, "{\"name\":\"ed\",\"city\":\"Philly\",\"barName\":\"2nd st\",\"drinkName\":\"beer\",\"shareCount\":0}");

         // verify via getbars
         RunTestScenario(phillyCity, edLogin, getBars, "[{\"Name\":\"ed\",\"City\":\"Philly\",\"BarName\":\"2nd st\",\"DrinkName\":\"beer\",\"ShareCount\":0}]");

         // add bar to phlly verify 2 bars via philly
         RunTestScenario(thirdsPhilly, edLogin, addBar, "{\"name\":\"ed\",\"city\":\"Philly\",\"barName\":\"3rd st\",\"drinkName\":\"wine\",\"shareCount\":0}");
   
         // add two bars to nyc
         RunTestScenario(secondstNCY, edLogin, addBar, "{\"name\":\"ed\",\"city\":\"NYC\",\"barName\":\"Sams\",\"drinkName\":\"beer\",\"shareCount\":0}");
         RunTestScenario(thirdsNYC, edLogin, addBar, "{\"name\":\"ed\",\"city\":\"NYC\",\"barName\":\"Petes\",\"drinkName\":\"wine\",\"shareCount\":0}");

         // verify 2 bars per city:
         RunTestScenario(phillyCity, edLogin, getBars, "[{\"Name\":\"ed\",\"City\":\"Philly\",\"BarName\":\"2nd st\",\"DrinkName\":\"beer\",\"ShareCount\":0},{\"Name\":\"ed\",\"City\":\"Philly\",\"BarName\":\"3rd st\",\"DrinkName\":\"wine\",\"ShareCount\":0}]");
         RunTestScenario(nycCity, edLogin, getBars, "[{\"Name\":\"ed\",\"City\":\"NYC\",\"BarName\":\"Sams\",\"DrinkName\":\"beer\",\"ShareCount\":0},{\"Name\":\"ed\",\"City\":\"NYC\",\"BarName\":\"Petes\",\"DrinkName\":\"wine\",\"ShareCount\":0}]");

         // delete nyc bars
         RunTestScenario(secondstNCY, edLogin, deleteBar, "{\"name\":\"ed\",\"city\":\"NYC\",\"barName\":\"Sams\",\"drinkName\":\"beer\",\"shareCount\":0}");
         RunTestScenario(thirdsNYC, edLogin, deleteBar, "{\"name\":\"ed\",\"city\":\"NYC\",\"barName\":\"Petes\",\"drinkName\":\"wine\",\"shareCount\":0}");

         // verfiy nyc bars gone
         RunTestScenario(nycCity, edLogin, getBars, "[]");

         // add 2 nyc bars 
         RunTestScenario(secondstNCY, edLogin, addBar, "{\"name\":\"ed\",\"city\":\"NYC\",\"barName\":\"Sams\",\"drinkName\":\"beer\",\"shareCount\":0}");
         RunTestScenario(thirdsNYC, edLogin, addBar, "{\"name\":\"ed\",\"city\":\"NYC\",\"barName\":\"Petes\",\"drinkName\":\"wine\",\"shareCount\":0}");

         // verify 2 bars  for NYC
         RunTestScenario(nycCity, edLogin, getBars, "[{\"Name\":\"ed\",\"City\":\"NYC\",\"BarName\":\"Sams\",\"DrinkName\":\"beer\",\"ShareCount\":0},{\"Name\":\"ed\",\"City\":\"NYC\",\"BarName\":\"Petes\",\"DrinkName\":\"wine\",\"ShareCount\":0}]");
         
         // verify 2 bars for Philly
         RunTestScenario(phillyCity, edLogin, getBars, "[{\"Name\":\"ed\",\"City\":\"Philly\",\"BarName\":\"2nd st\",\"DrinkName\":\"beer\",\"ShareCount\":0},{\"Name\":\"ed\",\"City\":\"Philly\",\"BarName\":\"3rd st\",\"DrinkName\":\"wine\",\"ShareCount\":0}]");

         // add fred
         RunTestScenario(fredModel, edLogin, addUser, "fred added");

         // add philly for fred
         RunTestScenario(phillyCity, fredLogin, addCity, "{\"name\":\"fred\",\"city\":\"Philly\",\"country\":\"USA\"}");

         // add bar in philly for fred that metches ed bar in philly
         RunTestScenario(secondstPhilly, fredLogin, addBar, "{\"name\":\"fred\",\"city\":\"Philly\",\"barName\":\"2nd st\",\"drinkName\":\"beer\",\"shareCount\":0}");

         // verify 1 bar matched for shared bard
         RunTestScenario(fredLogin, sharedBars, "[{\"BarName\":\"2nd st\",\"ShareCount\":1}]");

         // add second bar in philly that matches Ed
         RunTestScenario(thirdsPhilly, fredLogin, addBar, "{\"name\":\"fred\",\"city\":\"Philly\",\"barName\":\"3rd st\",\"drinkName\":\"wine\",\"shareCount\":0}");

         // verify shared bars shows two bars:
         RunTestScenario(fredLogin, sharedBars, "[{\"BarName\":\"2nd st\",\"ShareCount\":1},{\"BarName\":\"3rd st\",\"ShareCount\":1}]");

         // Add NYC city
         RunTestScenario(nycCity, fredLogin, addCity, "{\"name\":\"fred\",\"city\":\"NYC\",\"country\":\"USA\"}");

         // add nyc bar that matches 
         RunTestScenario(secondstNCY, fredLogin, addBar, "{\"name\":\"fred\",\"city\":\"NYC\",\"barName\":\"Sams\",\"drinkName\":\"beer\",\"shareCount\":0}");

         // add nyc bar that does not match ed's bars
         RunTestScenario(fouthstNCY, fredLogin, addBar, "{\"name\":\"fred\",\"city\":\"NYC\",\"barName\":\"Ralphs\",\"drinkName\":\"gin\",\"shareCount\":0}");

         // verify fred shares 3 bar 
         RunTestScenario(fredLogin, sharedBars, "[{\"BarName\":\"2nd st\",\"ShareCount\":1},{\"BarName\":\"3rd st\",\"ShareCount\":1},{\"BarName\":\"Sams\",\"ShareCount\":1}]");

         // log in as ed
         // veriify 3 shared bars as ed
         RunTestScenario(edLogin, sharedBars, "[{\"BarName\":\"2nd st\",\"ShareCount\":1},{\"BarName\":\"3rd st\",\"ShareCount\":1},{\"BarName\":\"Sams\",\"ShareCount\":1}]");
         
         // add ted
         RunTestScenario(tedModel, tedLogin, addUser, "ted added");

         // add philly
         RunTestScenario(phillyCity, tedLogin, addCity, "{\"name\":\"ted\",\"city\":\"Philly\",\"country\":\"USA\"}");

         // add bar that matches fred/ed in philly
         RunTestScenario(thirdsPhilly, tedLogin, addBar, "{\"name\":\"ted\",\"city\":\"Philly\",\"barName\":\"3rd st\",\"drinkName\":\"wine\",\"shareCount\":0}");
         
         // verify 1 bar with count 2 matches 
         RunTestScenario(tedLogin, sharedBars, "[{\"BarName\":\"3rd st\",\"ShareCount\":2}]");
      
         ClearDB();
      }

      #region HelperMethods
      private void RunTestScenario(object data, UserLogin login, string url, string result)
      {
         string response = string.Empty;
         response = MakeCall(url, data, login);

         Assert.Equal(result, response);
      }
      private void RunTestScenario(UserLogin login, string url, string result)
      {
         string response = string.Empty;
         response = MakeCall(url, login);
         Assert.Equal(result, response);
      }

      private string MakeCall(string url, object data, UserLogin userLogin)
      {
         var token = GetToken(userLogin.Name, userLogin.Password);
         using (var httpClient = new HttpClient())
         {
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("bearer", token);
            var response = httpClient.PostAsync(url, GetContent(data)).GetAwaiter().GetResult();
            return response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
         }
      }
      private string MakeCall(string url, UserLogin userLogin)
      {
         var token = GetToken(userLogin.Name, userLogin.Password);
         using (var httpClient = new HttpClient())
         {
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("bearer", token);
            var response = httpClient.PostAsync(url, GetContent()).GetAwaiter().GetResult();
            return response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
         }
      }
      private StringContent GetContent(object data)
      {
         var json = JsonConvert.SerializeObject(data);
         return new StringContent(json, UnicodeEncoding.UTF8, "application/json");
      }
      private StringContent GetContent()
      {
         return new StringContent("", UnicodeEncoding.UTF8, "application/json");
      }
      private string GetToken(string name, string password)
      {
         var userLogin = new UserLogin(name, password);
         using (var httpClient = new HttpClient())
         {
            var json = JsonConvert.SerializeObject(userLogin);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var response = httpClient.PostAsync($"{_url}/login", stringContent).GetAwaiter().GetResult();

           return response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
         }
      }

      private string GetToken(UserLogin login)
      {
         return GetToken(login.Name, login.Password);
      }
      
      private void ClearDB()
      {
         var dbHelper = new FortCode.DAL.DbHelper();
         dbHelper.ClearDBForTesting();
      }

      #endregion
   }
}
