﻿using System;
using System.Collections.Generic;
using System.Text;
using FortCode.Models;

namespace FortCodeIntegrationTest
{
   public class UserTestScenarios
   {
      public UserTestScenarios(object data, UserLogin login, string url, string result)
      {
         Data = data;
         Login = login;
         Url = url;
         Result = result;
      }

      public object Data { get; set; }
      public UserLogin Login { get; set; }
      public string Url { get; set; }
      public string Result { get; set; }
   }
}
