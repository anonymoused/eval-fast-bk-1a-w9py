﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
   public class UserLogin
   {
      public UserLogin(string name, string password)
      {
         Name = name;
         Password = password;
      }

      public UserLogin()
      {
         Name = string.Empty;
         Password = string.Empty;
      }

      public string Name { get; set; }
      public string Password { get; set; }
   }
}
