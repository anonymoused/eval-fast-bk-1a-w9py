﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
   public class CityInfo
   {
      public CityInfo()
      {
         Name = string.Empty;
         City = string.Empty;
         Country = string.Empty;
      }
      public CityInfo(string name, string city, string country)
      {
         Name = name;
         City = city;
         Country = country;
      }

      public CityInfo(string city, string country)
      {
         Name = string.Empty;
         City = city;
         Country = country;
      }

      public string Name { get; set; }
      public string City { get; set; }
      public string Country { get; set; }
   }
}
