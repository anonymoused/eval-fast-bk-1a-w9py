﻿using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;

namespace FortCode.DAL
{
   public class DbHelper : IDbHelper 
   {
      #region Ctor, Member vars

      private string DbName = "user_data";
      private string MasterDbName = "master";

      #endregion

      #region Public methods

      public Models.UserModel GetUserByLogin(Models.UserLogin userLogin)
      {
         Models.UserModel curModel = new Models.UserModel();

         System.Text.StringBuilder sb = new System.Text.StringBuilder();
         using (SqlConnection con = GetConnection(DbName))
         {
            con.Open();

            string sql = $"SELECT top 1 name, email from UserInfo WHERE Name = @name AND Password = @password";

            using(SqlCommand cmd = new SqlCommand(sql, con))
            {
               cmd.Parameters.Add("@name", System.Data.SqlDbType.NVarChar);
               cmd.Parameters.Add("@password", System.Data.SqlDbType.NVarChar);

               cmd.Parameters["@name"].Value = userLogin.Name;
               cmd.Parameters["@password"].Value = userLogin.Password;

               using (SqlDataReader reader = cmd.ExecuteReader())
               {
                  while (reader.Read())
                  {
                     curModel = new Models.UserModel();
                     curModel.Name = reader.GetString(0);
                     curModel.Email = reader.GetString(1);
                  }
               }
            }
         }

         return curModel;
      }


      public IEnumerable<Models.CityInfo> GetCityInfo(string name)
      {
         List<Models.CityInfo> cityList = new List<Models.CityInfo>();

         System.Text.StringBuilder sb = new System.Text.StringBuilder();
         using (SqlConnection con = GetConnection(DbName))
         {
            con.Open();

            string sql = $"SELECT city, country from CityInfo WHERE UserInfoId = (SELECT Id from UserInfo WHERE Name = @name)";

            using(SqlCommand cmd = new SqlCommand(sql, con))
            {
               cmd.Parameters.Add("@name", System.Data.SqlDbType.NVarChar);

               cmd.Parameters["@name"].Value = name;

               using (SqlDataReader reader = cmd.ExecuteReader())
               {
                  while (reader.Read())
                  {
                     Models.CityInfo ci = new Models.CityInfo
                     {
                        Name = name,
                        City = reader.GetString(0),
                        Country = reader.GetString(1)
                     };

                     cityList.Add(ci);
                  }
               }
            }
         }

         return cityList;
      }

      public void AddCityInfo(Models.CityInfo cityInfo)
      {
         System.Text.StringBuilder sb = new System.Text.StringBuilder();
         using (SqlConnection con = GetConnection(DbName))
         {
            con.Open();

            string sql = @"INSERT INTO CityInfo (UserInfoId, City, Country) 
                           VALUES ((SELECT Id 
                                      FROM UserInfo 
                                      WHERE name = @name), 
                                   @city, 
                                   @country)
            ";

            using(SqlCommand cmd = new SqlCommand(sql, con))
            {
               cmd.Parameters.Add("@name", System.Data.SqlDbType.NVarChar);
               cmd.Parameters.Add("@city", System.Data.SqlDbType.NVarChar);
               cmd.Parameters.Add("@country", System.Data.SqlDbType.NVarChar);

               cmd.Parameters["@name"].Value = cityInfo.Name;
               cmd.Parameters["@city"].Value = cityInfo.City;
               cmd.Parameters["@country"].Value = cityInfo.Country;

               cmd.ExecuteNonQuery();
            }
         }

         return;
      }
      public void DeleteCityInfo(Models.CityInfo cityInfo)
      {
         System.Text.StringBuilder sb = new System.Text.StringBuilder();
         using (SqlConnection con = GetConnection(DbName))
         {
            con.Open();

            string sql = @"
                DELETE CityInfo 
                 WHERE UserInfoId = (SELECT Id 
                                       FROM UserInfo 
                                      WHERE Name = @name
                                    )
                   AND City = @city";

            using(SqlCommand cmd = new SqlCommand(sql, con))
            {
               cmd.Parameters.Add("@name", System.Data.SqlDbType.NVarChar);
               cmd.Parameters.Add("@city", System.Data.SqlDbType.NVarChar);

               cmd.Parameters["@name"].Value = cityInfo.Name;
               cmd.Parameters["@city"].Value = cityInfo.City;

               cmd.ExecuteNonQuery();
            }
         }

         return;
      }

      public void AddBarinfo(Models.BarInfo barInfo)
      {
         System.Text.StringBuilder sb = new System.Text.StringBuilder();
         using (SqlConnection con = GetConnection(DbName))
         {
            con.Open();

            string sql = @"
                INSERT INTO BarInfo (CityInfoId, BarName, DrinkName) 
                       VALUES ( (SELECT Id 
                                   FROM CityInfo 
                                  WHERE City = @city 
                                    AND UserInfoId = (SELECT Id 
                                                        FROM UserInfo 
                                                       WHERE Name = @name
                                                      )
                                 ),
                                 @barname, 
                                 @drinkname
                              );
            ";

            using(SqlCommand cmd = new SqlCommand(sql, con))
            {
               cmd.Parameters.Add("@name", System.Data.SqlDbType.NVarChar);
               cmd.Parameters.Add("@city", System.Data.SqlDbType.NVarChar);
               cmd.Parameters.Add("@barname", System.Data.SqlDbType.NVarChar);
               cmd.Parameters.Add("@drinkname", System.Data.SqlDbType.NVarChar);

               cmd.Parameters["@name"].Value = barInfo.Name;
               cmd.Parameters["@city"].Value = barInfo.City;
               cmd.Parameters["@barname"].Value = barInfo.BarName;
               cmd.Parameters["@drinkname"].Value = barInfo.DrinkName;

               cmd.ExecuteNonQuery();
            }
         }

         return;
      }
      public void DeleteBarInfo(Models.BarInfo barInfo)
      {
         System.Text.StringBuilder sb = new System.Text.StringBuilder();
         using (SqlConnection con = GetConnection(DbName))
         {
            con.Open();

            string sql = @"
              DELETE BarInfo 
               WHERE CityInfoId = 
                         (SELECT Id 
                            FROM CityInfo 
                           WHERE City = @city
                             AND UserInfoId = (SELECT Id 
                                                   FROM UserInfo
                                                  WHERE Name = @name)
                         )
                 AND BarName = @barname
            ";

            using(SqlCommand cmd = new SqlCommand(sql, con))
            {
               cmd.Parameters.Add("@name", System.Data.SqlDbType.NVarChar);
               cmd.Parameters.Add("@city", System.Data.SqlDbType.NVarChar);
               cmd.Parameters.Add("@barname", System.Data.SqlDbType.NVarChar);

               cmd.Parameters["@name"].Value = barInfo.Name;
               cmd.Parameters["@city"].Value = barInfo.City;
               cmd.Parameters["@barname"].Value = barInfo.BarName;

               cmd.ExecuteNonQuery();
            }
         }

         return;
      }

      public IEnumerable<Models.BarInfo> GetBarInfo(string name, string city)
      {
         List<Models.BarInfo> barList = new List<Models.BarInfo>();

         using (SqlConnection con = GetConnection(DbName))
         {
            con.Open();

            string sql = @"
               SELECT b.BarName, 
                      b.DrinkName 
                 FROM BarInfo b 
                 JOIN CityInfo c on c.Id = b.CityInfoId 
                 JOIN UserInfo u on u.Id = c.UserInfoId  
                WHERE u.Name = @name
                  AND c.City = @city 
            ";

            using(SqlCommand cmd = new SqlCommand(sql, con))
            {
               cmd.Parameters.Add("@name", System.Data.SqlDbType.NVarChar);
               cmd.Parameters.Add("@city", System.Data.SqlDbType.NVarChar);

               cmd.Parameters["@name"].Value = name;
               cmd.Parameters["@city"].Value = city;

               using (SqlDataReader reader = cmd.ExecuteReader())
               {
                  while (reader.Read())
                  {
                     Models.BarInfo ci = new Models.BarInfo
                     {
                        Name = name,
                        City = city,
                        BarName = reader.GetString(0),
                        DrinkName = reader.GetString(1)
                     };

                     barList.Add(ci);
                  }
               }
            }
         }

         return barList;
      }

      public IEnumerable<Models.SharedBars> GetSharedBarInfo(string name)
      {
         List<Models.SharedBars> barList = new List<Models.SharedBars>();

         System.Text.StringBuilder sb = new System.Text.StringBuilder();
         using (SqlConnection con = GetConnection(DbName))
         {
            con.Open();

            string sql = @"
               SELECT b.BarName, count(u.Name)
                 FROM UserInfo u
                 JOIN CityInfo c on u.Id = c.UserInfoId
                 JOIN BarInfo b on c.Id = b.CityInfoId
                WHERE u.Name <> @name
                  AND EXISTS (SELECT bb.Id
                                FROM BarInfo bb
                                JOIN CityInfo cc on cc.Id = bb.CityInfoId
                                JOIN UserInfo uu on uu.Id = cc.UserInfoId
                               WHERE bb.BarName = b.BarName
                                 AND cc.City = c.City
                                 AND uu.Name = @name
                             )
                GROUP BY BarName
               ";

            using(SqlCommand cmd = new SqlCommand(sql, con))
            {
               cmd.Parameters.Add("@name", System.Data.SqlDbType.NVarChar);

               cmd.Parameters["@name"].Value = name;

               using (SqlDataReader reader = cmd.ExecuteReader())
               {
                  while (reader.Read())
                  {
                     Models.SharedBars ci = new Models.SharedBars
                     {
                        BarName = reader.GetString(0),
                        ShareCount = reader.GetInt32(1)
                     };

                     barList.Add(ci);
                  }
               }
            }
         }

         return barList;
      }

      public void StoreUser(Models.UserModel userModel)
      {
         System.Text.StringBuilder sb = new System.Text.StringBuilder();
         using (SqlConnection con = GetConnection(DbName))
         {
            con.Open();

            string sql = $"INSERT INTO UserInfo (Name, Password, Email) VALUES(@name, @password, @email)";

            using(SqlCommand cmd = new SqlCommand(sql, con))
            {
               cmd.Parameters.Add("@name", System.Data.SqlDbType.NVarChar);
               cmd.Parameters.Add("@password", System.Data.SqlDbType.NVarChar);
               cmd.Parameters.Add("@email", System.Data.SqlDbType.NVarChar);

               cmd.Parameters["@name"].Value = userModel.Name;
               cmd.Parameters["@password"].Value = userModel.Password;
               cmd.Parameters["@email"].Value = userModel.Email;

               cmd.ExecuteNonQuery();
            }
         }
      }

      public void SetUpDB()
      {
         if (DatabaseExists())
            return;

         // now need to create some tables
         using (SqlConnection con = GetConnection(MasterDbName))
         {
            con.Open();

            string sql = $"CREATE database {DbName}";

            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
               cmd.ExecuteNonQuery();
            }
         }

         using (SqlConnection con = GetConnection(DbName))
         {
            con.Open();

            string sql = @"
              CREATE TABLE dbo.UserInfo (
                 Id INT NOT NULL IDENTITY PRIMARY KEY,
                 Name NVARCHAR(200) NOT NULL,
                 Password NVARCHAR(200) NOT NULL,
                 Email NVARCHAR(200) NOT NULL
              );";

            RunNonQuerySqlCommand(sql, con);


            sql = @"
              CREATE TABLE dbo.CityInfo (
                 Id INT NOT NULL IDENTITY PRIMARY KEY,
                 City NVARCHAR(200) NOT NULL,
                 Country NVARCHAR(200),
                 UserInfoId int NOT NULL
              );";

            RunNonQuerySqlCommand(sql, con);

            sql = @"
              ALTER TABLE dbo.CityInfo
              ADD CONSTRAINT FK_CityInfo_Name FOREIGN KEY (UserInfoId)
              REFERENCES dbo.UserInfo (Id)
              ON DELETE CASCADE
              ON UPDATE CASCADE; 
            ";

            RunNonQuerySqlCommand(sql, con);

            sql = @"
              CREATE TABLE dbo.BarInfo (
                 Id INT NOT NULL IDENTITY PRIMARY KEY,
                 CityInfoId int NOT NULL,
                 BarName NVARCHAR(200) NOT NULL,
                 DrinkName NVARCHAR(200)
              );";
            
            RunNonQuerySqlCommand(sql, con);

            sql = @"
              ALTER TABLE dbo.BarInfo
              ADD CONSTRAINT FK_BarInfo_City FOREIGN KEY (CityInfoId)
              REFERENCES dbo.CityInfo (Id)
              ON DELETE CASCADE
              ON UPDATE CASCADE; 
             ";
            
            RunNonQuerySqlCommand(sql, con);
         }
      }

      
      public void ClearDBForTesting()
      {
         using (SqlConnection con = GetConnection(DbName))
         {
            con.Open();

            string sql = $"DELETE FROM UserInfo WHERE Id > 0";

            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
               cmd.ExecuteNonQuery();
            }
         }
      }

      #endregion

      #region Private methods

      private void RunNonQuerySqlCommand(string sql, SqlConnection con)
      {
         using (SqlCommand cmd = new SqlCommand(sql, con))
         {
            cmd.ExecuteNonQuery();
         }
      }
    
      private SqlConnection GetConnection(string dbName)
      {
         //Data Source=127.0.0.1:8181;User ID=sa;Password=********;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False/
         SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
         csb.IntegratedSecurity = false;
         csb.TrustServerCertificate = true;
         csb.InitialCatalog = dbName;

         var docker_db_name = Environment.GetEnvironmentVariable("FORTCODEENV_USER");
         var docker_db_password = Environment.GetEnvironmentVariable("FORTCODEENV_PASSWORD");
         var docker_db_server = Environment.GetEnvironmentVariable("FORTCODEENV_SERVER");

         // conceit for running outside of a docker container // DELETE ME 
         csb.UserID = string.IsNullOrEmpty(docker_db_name) ? "sa" : docker_db_name;
         csb.Password = string.IsNullOrEmpty(docker_db_password) ? "Tsrb##Tsrb22" : docker_db_password;
         csb.DataSource = string.IsNullOrEmpty(docker_db_server) ? "127.0.0.1,8181" : docker_db_server;
         
         return new SqlConnection(csb.ConnectionString);
      }

      private bool DatabaseExists()
      {
         int dbId = 0;
         using (SqlConnection con = GetConnection(MasterDbName))
         {
            con.Open();

            string sql = $"IF DB_ID('{DbName}') IS NOT NULL SELECT 1 ELSE SELECT 0;";

            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
               using (SqlDataReader reader = cmd.ExecuteReader())
               {
                  while (reader.Read())
                     dbId = reader.GetInt32(0);
               }
            }
         }

         return dbId > 0;
      }

      #endregion
   }
}
