﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using FortCode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace FortCode.controllers
{
   [Route("api/[controller]")]
   [ApiController]
   public class UserController : ControllerBase
   {

      [HttpPost("adduser")]
      public IActionResult StoreUser([FromBody] UserModel userModel)
      {
         DAL.IDbHelper dbHelper = DAL.DataFactory.GetDataHelper();
         BL.UserService userService = new BL.UserService(dbHelper);
         userService.StoreUser(userModel);

         return Ok($"{userModel.Name} added");
      }

      [HttpPost("getcities")]
      [Authorize]
      public IActionResult GetCities()
      {
         string name = GetCurrentUser() ?? string.Empty;
         DAL.IDbHelper dbHelper = DAL.DataFactory.GetDataHelper();
         BL.UserService userService = new BL.UserService(dbHelper);
         string cityInfo = userService.GetCitiesForUser(name);

         return Ok(cityInfo);
      }

      [HttpPost("addcity")]
      [Authorize]
      public IActionResult AddCity([FromBody] Models.CityInfo cityInfo)
      {
         cityInfo.Name = GetCurrentUser() ?? string.Empty;
         DAL.IDbHelper dbHelper = DAL.DataFactory.GetDataHelper();
         BL.UserService userService = new BL.UserService(dbHelper);
         userService.AddCity(cityInfo);

         return Ok(cityInfo);
      }

      [HttpPost("deletecity")]
      [Authorize]
      public IActionResult DeleteCity([FromBody] Models.CityInfo cityInfo)
      {
         cityInfo.Name = GetCurrentUser() ?? string.Empty;
         DAL.IDbHelper dbHelper = DAL.DataFactory.GetDataHelper();
         BL.UserService userService = new BL.UserService(dbHelper);
         userService.DeleteCity(cityInfo);

         return Ok(cityInfo);
      }

      [HttpPost("addbar")]
      [Authorize]
      public IActionResult AddBar([FromBody] Models.BarInfo barInfo)
      {
         barInfo.Name = GetCurrentUser() ?? string.Empty;
         DAL.IDbHelper dbHelper = DAL.DataFactory.GetDataHelper();
         BL.UserService userService = new BL.UserService(dbHelper);
         userService.AddBarInfo(barInfo);

         return Ok(barInfo);
      }

      [HttpPost("deletebar")]
      [Authorize]
      public IActionResult DeleteBar([FromBody] Models.BarInfo barInfo)
      {
         barInfo.Name = GetCurrentUser() ?? string.Empty;
         DAL.IDbHelper dbHelper = DAL.DataFactory.GetDataHelper();
         BL.UserService userService = new BL.UserService(dbHelper);
         userService.DeleteBarInfo(barInfo);

         return Ok(barInfo);
      }
      
      [HttpPost("getbars")]
      [Authorize]
      public IActionResult GetBar([FromBody] Models.BarInfo barInfo)
      {
         barInfo.Name = GetCurrentUser() ?? string.Empty;
         DAL.IDbHelper dbHelper = DAL.DataFactory.GetDataHelper();
         BL.UserService userService = new BL.UserService(dbHelper);
         string barInfoList = userService.GetBarInfo(barInfo.Name, barInfo.City);

         return Ok(barInfoList);
      }
      
      [HttpPost("sharedbars")]
      [Authorize]
      public IActionResult SharedBar()
      {
         string name = GetCurrentUser() ?? string.Empty;
         DAL.IDbHelper dbHelper = DAL.DataFactory.GetDataHelper();
         BL.UserService userService = new BL.UserService(dbHelper);
         string barInfo = userService.SharedBarInfo(name);

         return Ok(barInfo);
      }

      private string? GetCurrentUser()
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity != null)
         {
            var userCliams = identity.Claims;

            return userCliams.FirstOrDefault(o => o.Type == ClaimTypes.NameIdentifier)?.Value ?? null;
         }

         return null;
      }
   }
}
