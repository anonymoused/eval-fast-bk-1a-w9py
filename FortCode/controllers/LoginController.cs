﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Models;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

namespace FortCode.controllers
{
   [Route("api/[controller]")]
   [ApiController]
   public class LoginController : ControllerBase
   {
      private IConfiguration _config;

      public LoginController(IConfiguration config)
      {
         _config = config;
      }

      [AllowAnonymous]
      [HttpPost]
      public IActionResult Login([FromBody] UserLogin userLogin)
      {
         var user = Authenticate(userLogin);

         if (user != null)
         {
            var token = Generate(user);
            return Ok(token);
         }

         return NotFound("User not found");
      }

      private String Generate(UserModel userModel)
      {
         var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
         var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

         var claims = new[]
         {
             new Claim(ClaimTypes.NameIdentifier, userModel.Name)
         };

         var token = new JwtSecurityToken(_config["Jwt:Issuer"],
            _config["Jwt:Audiece"],
            claims,
            expires: DateTime.Now.AddMinutes(15),
            signingCredentials: credentials);

         return new JwtSecurityTokenHandler().WriteToken(token);
      }

      private UserModel? Authenticate(UserLogin userLogin)
      {
         DAL.IDbHelper dbHelper = DAL.DataFactory.GetDataHelper();
         BL.UserService userService = new BL.UserService(dbHelper);
         return userService.Authenticate(userLogin);
      }
   }
}
