﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FortCode.BL
{
   public class UserService
   {
      private DAL.IDbHelper _dbHelper;

      public UserService(DAL.IDbHelper dbHelper)
      {
         _dbHelper = dbHelper;
      }
      
      public Models.UserModel? Authenticate(Models.UserLogin userLogin)
      {
         Models.UserModel curModel = _dbHelper.GetUserByLogin(userLogin);

         if (string.IsNullOrEmpty(curModel.Name))
            return null;
         else
            return curModel;
      }

      public void StoreUser(Models.UserModel userModel)
      {
         _dbHelper.StoreUser(userModel);
      }

      public string GetCitiesForUser(string name)
      {
         var cityList = _dbHelper.GetCityInfo(name);

         return JsonConvert.SerializeObject(cityList);
      }

      public void AddCity(Models.CityInfo cityInfo)
      {
         _dbHelper.AddCityInfo(cityInfo);
      }

      public void DeleteCity(Models.CityInfo cityInfo)
      {
         _dbHelper.DeleteCityInfo(cityInfo);
      }

      public void AddBarInfo(Models.BarInfo barInfo)
      {
         _dbHelper.AddBarinfo(barInfo);
      }

      public void DeleteBarInfo(Models.BarInfo barInfo)
      {
         _dbHelper.DeleteBarInfo(barInfo);
      }

      public string GetBarInfo(string name, string city)
      {
         var barInfoList = _dbHelper.GetBarInfo(name, city);

         return JsonConvert.SerializeObject(barInfoList);
      }

      public string SharedBarInfo(string name)
      {
         var barInfoList = _dbHelper.GetSharedBarInfo(name);

         return JsonConvert.SerializeObject(barInfoList);
      }
   }
}
