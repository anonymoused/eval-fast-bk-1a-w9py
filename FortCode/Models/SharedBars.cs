﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
   public class SharedBars
   {
      public SharedBars()
      {
         BarName = string.Empty;
         ShareCount = 0;
      }

      public string BarName { get; set; }
      public int ShareCount { get; set; }
   }
}
