﻿# FortCode project API documentation

## Setup
To setup the project run `docker-compose up` to start the containers. The containers will create endpoints at `http://localhost:8100/api/`. 

All requests are HTTP POST requests. 

Authentication is handled by JWT which requires requests to submit a bearer token returned by the `/api/login` endpoint. 
The token is set to be valid for fifteen minutes. 
The API was test using the Postman application which support.

## Endpoints

### Add User
_Endpoint_:

```
/api/user/adduser
```

_Body_:
```
{"name":"ted", "password":"fizz", "email":"ted#junk.blah"}
```

_Response_:
```
ed added
```

### Login:
_Endpoint_:

```
/api/login
```

_Body_:
```
{"name":"ed", "password":"password"}
```

_Response_:
A JWT token is returned. This token is used with all endpoints except `adduser` and should be provided as an authentication header of type `bearer`. 
```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6ImVkIiwiZXhwIjoxNjQzNDgwNzA3LCJpc3MiOiJsb2NhbGhvc3Q6ODEwMC8iLCJhdWQiOiJsb2NhbGhvc3Q6ODEwMC8ifQ.Lc2nwWBSWUADadAm11VYXi17y4-H87tdE9SWI6wo1oI
```

### Add City:
_Endoint_:

```
/api/user/addcity
```

_Body_:
```
{"city":"Philly", "country":"USA"}
```

_Response_:
```
{"city":"Philly", "country":"USA"}
```

### Get Cities:
_Endoint_:

```
/api/user/getcities
```

_Body_:
No body parameters provided: the name is obtained from the token.

_Response_:
```
[{"Name":"ed","City":"Philly","Country":"USA"}]
```

### Delete City:
_Endoint_:

```
/api/user/deletecity
```

_Body_:
```
{"city":"Philly"}
```

_Response_:
```
{"name":"ed","city":"Philly","country":""}
```

### Add Bar:
_Endoint_:

```
/api/user/addbar
```

_Body_:
```
{"city":"Philly", "barname":"3rd st", "drinkname":"G&T"}
```

_Response_:
```
{"name":"ed","city":"Philly","barName":"3rd st","drinkName":"G&T","shareCount":0}
```

### GetBars:
_Endoint_:

```
/api/user/getbars
```

_Body_:
```
{"city":"Philly"}
```

_Response_:
```
[{"Name":"ed","City":"Philly","BarName":"3rd st","DrinkName":"G&T","ShareCount":0}]
```

### Delete Bar:
_Endoint_:

```
/api/user/deletebar
```

_Body_:
```
{"city":"NYC", "barname":"3rd st"}
```

_Response_:
```
{"name":"ed","city":"Philly","barName":"3rd st","drinkName":"","shareCount":0}
```

### Shared Bars:
_Endoint_:

```
/api/user/sharedbars	
```

_Body_:
No body parameters provided: the name is obtained from the token.

_Response_:
```
[{"BarName":"3rd st","ShareCount":1}]
```
