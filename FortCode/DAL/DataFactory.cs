﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.DAL
{
   public static class DataFactory
   {
      public static IDbHelper GetDataHelper()
      {
         return new DbHelper();
      }
   }
}
