﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
   public class UserModel
   {
      public UserModel()
      {
         Name = string.Empty;
         Password = string.Empty;
         Email = string.Empty;
      }

      public UserModel(string name, string password, string email)
      {
         Name = name;
         Password = password;
         Email = email;
      }

      public string Name { get; set; }
      public string Password { get; set; }
      public string Email { get; set; }
   }
}
