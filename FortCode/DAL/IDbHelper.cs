﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.DAL
{
   public interface IDbHelper
   {
      void SetUpDB();
      Models.UserModel GetUserByLogin(Models.UserLogin userLogin);

      void StoreUser(Models.UserModel userModel);

      IEnumerable<Models.CityInfo> GetCityInfo(string userName);
      void AddCityInfo(Models.CityInfo cityInfo);
      void DeleteCityInfo(Models.CityInfo cityInfo);
      void AddBarinfo(Models.BarInfo barInfo);
      void DeleteBarInfo(Models.BarInfo barInfo);
      IEnumerable<Models.BarInfo> GetBarInfo(string name, string city);
      IEnumerable<Models.SharedBars> GetSharedBarInfo(string name);

      void ClearDBForTesting();
   }
}
