﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
   public class BarInfo
   {
      public BarInfo(string city, string barname, string drinkname)
      {
         Name = string.Empty;
         City = city;
         BarName = barname;
         DrinkName = drinkname;
         ShareCount = 0;
      }
      public BarInfo()
      {
         Name = string.Empty;
         City = string.Empty;
         BarName = string.Empty;
         DrinkName = string.Empty;
         ShareCount = 0;
      }

      public string Name { get; set; }
      public string City { get; set; }
      public string BarName { get; set; }
      public string DrinkName { get; set; }
      public int ShareCount { get; set;  }
   }
}
